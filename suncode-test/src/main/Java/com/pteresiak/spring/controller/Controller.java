package com.pteresiak.spring.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParser;
import com.google.gson.Gson;
import com.pteresiak.spring.component.ChooseColumnPOJO;
import com.pteresiak.spring.component.TestTableDao;

@RestController
public class Controller {

	@Autowired
	TestTableDao dataBaseDao;
	
	@Autowired
	ChooseColumnPOJO chooseColumnPOJO;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView startPage() {
		ModelAndView mvc = new ModelAndView("index");
		List repeatedData =dataBaseDao.getAllDataFromTestTable();
		Gson gson = new Gson();
		String JSON = gson.toJson(repeatedData);
		
		mvc.addObject("testDB",JSON);
		mvc.addObject("chooseColumnPOJO",chooseColumnPOJO);
		return mvc;
	};

	@RequestMapping(value = "/testDB", method = RequestMethod.GET)
	public String testDB(){
		return dataBaseDao.getRepeatedRow("kolumna1").toString();
	}
	
	@RequestMapping(value = "/testForm", method = RequestMethod.POST)
	public String testForm(@ModelAttribute ChooseColumnPOJO chooseColumnPOJO){
		List repeatedData =dataBaseDao.getRepeatedRow(chooseColumnPOJO.getColumnName());
		Gson gson = new Gson();
		String JSON = gson.toJson(repeatedData);
		return JSON;
	}
	
	@RequestMapping(value = "{column}/repeatedRow.json", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public String getRepeatedJSON(@PathVariable String column){
		List repeatedData =dataBaseDao.getRepeatedRow(column);
		Gson gson = new Gson();
		String JSON = gson.toJson(repeatedData);
		return JSON;
	}
	
	@RequestMapping(value = "{column}/noRepeatedRow.json", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public String getNoRepeatedJSON(@PathVariable String column){
		List repeatedData =dataBaseDao.getNoRepeatedRow(column);
		Gson gson = new Gson();
		String JSON = gson.toJson(repeatedData);
		return JSON;
	}
	
}
