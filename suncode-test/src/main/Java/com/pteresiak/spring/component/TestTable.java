package com.pteresiak.spring.component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tabela_testowa")
public class TestTable {
	@Id
	private Long id;
	@Column(name = "kolumna1")
	private String Column1;
	@Column(name = "kolumna2")
	private String Column2;
	@Column(name = "kolumna3")
	private String Column3;
	@Column(name = "kolumna4")
	private Long Column4;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColumn1() {
		return Column1;
	}

	public void setColumn1(String column1) {
		Column1 = column1;
	}

	public String getColumn2() {
		return Column2;
	}

	public void setColumn2(String column2) {
		Column2 = column2;
	}

	public String getColumn3() {
		return Column3;
	}

	public void setColumn3(String column3) {
		Column3 = column3;
	}

	public Long getColumn4() {
		return Column4;
	}

	public void setColumn4(Long column4) {
		Column4 = column4;
	}

	@Override
	public String toString() {
		return "\nTestTable [id=" + id + ", Column1=" + Column1 + ", Column2=" + Column2 + ", Column3=" + Column3
				+ ", Column4=" + Column4 + "]";
	}

}
