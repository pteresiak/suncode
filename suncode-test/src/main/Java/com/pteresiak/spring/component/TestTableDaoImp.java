package com.pteresiak.spring.component;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public class TestTableDaoImp extends HibernateDaoSupport implements TestTableDao {

	@Autowired
	public void setSessionFactoryBean(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public List getAllDataFromTestTable() {
		return getSessionFactory().getCurrentSession().createSQLQuery("SELECT * FROM tabela_testowa")
				.addEntity(TestTable.class).list();

	}

	@Override
	public List getRepeatedRow(String columnName) {
		return getSessionFactory().getCurrentSession()
				.createSQLQuery(
						"SELECT * FROM tabela_testowa WHERE "+columnName+" IN (SELECT "+columnName+" FROM tabela_testowa GROUP BY "+columnName+" HAVING (COUNT( "+columnName+" )>1))")
				.addEntity(TestTable.class).list();

	}

	@Override
	public List getNoRepeatedRow(String columnName) {
		return getSessionFactory().getCurrentSession()
				.createSQLQuery(
						"SELECT * FROM tabela_testowa WHERE "+columnName+" IN (SELECT "+columnName+" FROM tabela_testowa GROUP BY "+columnName+" HAVING (COUNT( "+columnName+" )=1))")
				.addEntity(TestTable.class).list();
	}

}
