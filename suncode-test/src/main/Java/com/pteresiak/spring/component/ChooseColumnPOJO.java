package com.pteresiak.spring.component;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class ChooseColumnPOJO {

	private String columnName;
	private static List<String> listColumn = null;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public List<String> getListColumn() {
		if (listColumn == null) {
			ChooseColumnPOJO.initList();
		}
		return ChooseColumnPOJO.listColumn;
	}

	private static void initList() {

		ChooseColumnPOJO.listColumn = new ArrayList<>();
		ChooseColumnPOJO.listColumn.add("kolumna1");
		ChooseColumnPOJO.listColumn.add("kolumna2");
		ChooseColumnPOJO.listColumn.add("kolumna3");
		ChooseColumnPOJO.listColumn.add("kolumna4");

	}

}
