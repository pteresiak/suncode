package com.pteresiak.spring.component;

import java.util.List;

public interface TestTableDao {
	public List getAllDataFromTestTable();

	public List getRepeatedRow(String columnName);

	public List getNoRepeatedRow(String columnName);

}
