<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">


<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description"
	content="SUncode-test:Show data from external DB">
<meta name="author" content="Piotr Teresiak">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Suncode Test</title>
<spring:url value="/resources/css/bootstrap.min.css" var="bootstrap_css"></spring:url>
<spring:url value="/resources/css/main.css" var="main_css"></spring:url>
<spring:url value="/resources/js/jquery-3.1.0.min.js" var="jQuery"></spring:url>
<spring:url value="/resources/js/bootstrap.min.js" var="bootstrap_js"></spring:url>
<spring:url value="/resources/css/normalize.css" var="normalize_css"></spring:url>
<spring:url value="/resources/css/font-awesome.css" var="font_css"></spring:url>
<spring:url value="/resources/js/index.js" var="index_js"></spring:url>

<!-- Normalize CSS -->
<link href="${normalize_css}" rel="stylesheet">
<!-- Fonts CSS -->
<link href="${font_css}" rel="stylesheet">

<!-- Jquery -->
<script src="${jQuery}"></script>
<!-- Bootstrap core CSS -->
<link href="${bootstrap_css}" rel="stylesheet">
<!-- Main CSS -->
<link href="${main_css}" rel="stylesheet">
</head>

<body>
	<h1>Hello Suncode!</h1>

	<div class="master">
		<div class="combobox">
			<spring:url value="/testForm" var="test"></spring:url>
			<form:form method="POST" modelAttribute="chooseColumnPOJO"
				action="${test}">
				<div class="selectList form-inline">
					<h2>
						<form:select id="selectColumn" path="columnName"
							class="selectpicker  btn-primary btn-lg ">
							<form:options items="${chooseColumnPOJO.listColumn}" />
						</form:select>
					</h2>
				</div>
				<div class="button form-inline">
					<h2>
						<form:button class="btn btn-primary btn-lg show" type="button">Show</form:button>
					</h2>
				</div>
			</form:form>


		</div>
		<div class="resoult"></div>
	</div>



	<!-- Bootstrap JS -->
	<script src="${bootstrap_js}"></script>
	<!-- Index JS -->
	<script src="${index_js}"></script>
</body>
</html>
