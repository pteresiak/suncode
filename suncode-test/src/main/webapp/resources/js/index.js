/**
 * 
 */

$(document).ready(
		function() {

			$(".show").click(
					function() {
						$(".resoult").empty();
						var selectedColumn = $("#selectColumn option:selected")
								.text();

						$.getJSON(selectedColumn + "/repeatedRow.json",
								function(rawRepeatedJSON) {

									addTable(rawRepeatedJSON, "Repeat Data by:"
											+ selectedColumn);
								});
						$
								.getJSON(
										selectedColumn + "/noRepeatedRow.json",
										function(rawRepeatedJSON) {

											console.log(rawRepeatedJSON);
											addTable(rawRepeatedJSON,
													"Unique Data by: "
															+ selectedColumn);
										});
					});
		});

var addTable = function(JSONArray, title) {

	var table = $("<table>");
	table.addClass("table");
	table.addClass("table-bordered");

	var headerTable = "<tr class='table-bordered header'> "
			+ "<td><h3>ID</h3></td>" + "<td><h3>Kolumna1</h3></td>"
			+ "<td><h3>Kolumna2</h3></td>" + "<td><h3>Kolumna3</h3></td>"
			+ "<td><h3>Kolumna4</h3></td></tr>";

	var data = "";
	$.each(JSONArray, function(key, val) {
		var id = "<td>" + "<h4>" + val.id + "</h4>" + "</td>";
		var column1 = "<td>" + "<h4>" + val.Column1 + "</h4>"
		"</td>";
		var column2 = "<td>" + "<h4>" + val.Column2 + "</h4>" + "</td>";
		var column3 = "<td>" + "<h4>" + val.Column3 + "</h4>" + "</td>";
		var column4 = "<td>" + "<h4>" + val.Column4 + "</h4>" + "</td>";
		data += "<tr class='table-bordered'>" + id + column1 + column2
				+ column3 + column4 + "</tr>";

	});

	table.append(headerTable).append(data);
	$(".resoult").append("<h2>" + title + "</h2>").append(table)
}
